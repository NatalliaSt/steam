package com.steam.pages;

import com.steam.framework.elements.Button;
import com.steam.framework.core.BasePage;
import com.steam.framework.elements.Label;
import org.openqa.selenium.By;

public class AppPage extends BasePage {
    private static String pageLocator = "//body/div[@class='responsive_page_frame with_header']";

    private Button buttonInstallSteam = new Button(By.xpath(String.format("//a[contains(text(),\"%s\")]", getLocale("loc.about-form.install-steam"))), "install Steam");
    private Label labelAssert = new Label(By.xpath("//div[@class='apphub_AppName']"), "assert");

    public AppPage() {
        super(By.xpath(pageLocator), "app page");
    }

    public void gamePageAssert() {
        assert (labelAssert.isDisplayed());
    }

    public void openInstallSteamPage() {
        buttonInstallSteam.click();
    }
}
