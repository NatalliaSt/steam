package com.steam.pages;

import com.steam.framework.core.BasePage;
import com.steam.framework.elements.Button;
import com.steam.framework.elements.Label;
import com.steam.framework.elements.Select;
import org.openqa.selenium.By;

public class AgeVerifyPage extends BasePage {
    private static String pageLocator = "//body/div[@class='responsive_page_frame with_header']";
    private Label labelAgegate = new Label(By.xpath("//div[@class='agegate_text_container']/h2"), "agegate");
    private Select selectYear = new Select(By.id("ageYear"), "Year");
    private Button buttonViewPage = new Button(By.xpath(String.format("//div[@class='agegate_text_container btns']//span[contains(text(),'%s')]", getLocale("loc.app-form.view-page"))), "view Page");


    public AgeVerifyPage() {
        super(By.xpath(pageLocator), "age verify page");
    }

    public void enterDOB() {
        if (labelAgegate.isDisplayed()) {
            selectYear.selectByVisibleText(getLocale("loc.app-form.dob-year"));
            buttonViewPage.click();
        }
    }
}
