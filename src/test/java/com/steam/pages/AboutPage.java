package com.steam.pages;

import com.steam.framework.elements.Button;
import com.steam.framework.core.BasePage;
import org.openqa.selenium.By;

public class AboutPage extends BasePage {
    private static String pageLocator = "//body/div[@class='responsive_page_frame with_header']";

    private Button buttonDownload = new Button(By.xpath("//div[@class=\"about_install_wrapper\"]//a[@class='about_install_steam_link']"), "download");

    public AboutPage() {
        super(By.xpath(pageLocator), "About page");
    }

    public String getDownloadURL() {
        return buttonDownload.getElement().getAttribute("href");
    }
}
