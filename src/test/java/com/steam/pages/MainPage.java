package com.steam.pages;

import com.steam.framework.elements.Button;
import com.steam.framework.core.BasePage;
import org.openqa.selenium.By;

public class MainPage extends BasePage {

    private static String pageLocator ="//body/div[@class='responsive_page_frame with_header']";
    private String xpathMenuItem = "//div[@class='store_nav_bg']//a[contains(text(),'%s') and @class='pulldown_desktop']";
    private String xpathSubMenuItem = "//div[@class='store_nav_bg']//a[contains(text(),'%s') and @class='popup_menu_item']";
    private String xpathLangButton = "//a[@class='popup_menu_item tight' and contains(text(),'%s')]";
    private Button buttonLanguageSelect = new Button(By.id("language_pulldown"),"language Select");

    public MainPage() {
        super(By.xpath(pageLocator), "mane page");
    }

    public void choseLanguage(String language) {
        buttonLanguageSelect.click();
        String xpath = String.format(xpathLangButton, language);
        Button languageButton = new Button (By.xpath(xpath),"Language Button");
        if (languageButton.isDisplayed()) {
            languageButton.click();
        }
    }

    public void navigateMenu(String itemName, String subItemName) {
        this.selectMenuItem(itemName);
        this.selectSubMenuItem(subItemName);
    }

    private void selectMenuItem(String itemName) {
        String xpath = String.format(xpathMenuItem, itemName);
        Button menuItemButton = new Button (By.xpath(xpath),"menu Item Button");
        menuItemButton.click();
    }

    private void selectSubMenuItem(String itemName) {
        String xpath = String.format(xpathSubMenuItem, itemName);
        Button menuItemButton = new Button (By.xpath(xpath),"sub menu Item Button");
        menuItemButton.clickAndWait();
    }
}
