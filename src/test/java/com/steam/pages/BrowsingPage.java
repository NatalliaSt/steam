package com.steam.pages;

import com.steam.framework.elements.Button;
import com.steam.framework.core.BasePage;
import com.steam.framework.elements.CollectionUIElements;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BrowsingPage extends BasePage {
    private static String pageLocator = String.format("//h2[@class='pageheader' and contains(text(), '%s')]", getLocale("loc.browsing-form.browsing"));
    private String xpathGameList = "//div[@class='contenthub_specials_ctn']//div[@class='discount_pct']";

    public BrowsingPage() {
        super(By.xpath(pageLocator), "Browsing page");
    }

    public void choseMaxDiscountRecommendedSpecialsGame() {
        List<Button> collectionUIElements =  CollectionUIElements.getCollectionUIElements(By.xpath(xpathGameList), "Button", Button.class);

        Button maxDiscountElement = collectionUIElements.get(0);
        List<Button> maxDiscountElements = new ArrayList<Button>();
        maxDiscountElements.add(collectionUIElements.get(0));
        for (int i = 1; i < collectionUIElements.size(); i++) {
            Button item = collectionUIElements.get(i);
            if (getDiscount(maxDiscountElement) > getDiscount(item)) {
                maxDiscountElement = item;
                maxDiscountElements.clear();
                maxDiscountElements.add(item);
            } else if (getDiscount(maxDiscountElement) == getDiscount(item)) {
                maxDiscountElements.add(item);
            }
        }
        Collections.shuffle(maxDiscountElements);
        maxDiscountElement = maxDiscountElements.get(0);
        maxDiscountElement.clickAndWait();
    }

    private float getDiscount(Button button) {
        String str = button.getText();
        str = str.replace("%", "");
        return Float.parseFloat(str);
    }
}
