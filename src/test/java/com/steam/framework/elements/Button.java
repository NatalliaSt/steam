package com.steam.framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;

public class Button extends BaseUIElement {

    /**
     *
     * @param locator
     * @param name
     */
    public Button(final By locator, final String name) {
        super(locator, name);
    }

    public Button(final RemoteWebElement element, final String name ) {
        super(element, name);
    }



}
