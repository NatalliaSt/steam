package com.steam.framework.elements;

import com.steam.framework.core.BaseEntity;
import com.steam.framework.browser.Browser;
import com.steam.framework.utils.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CollectionUIElements extends BaseEntity {

    public static <T> List<T> getCollectionUIElements(By locator, String name, Class tClass) {
        List<T> elementsUIList = new ArrayList<T>();
        WebDriverWait wait = new WebDriverWait(Browser.getInstance().getDriver(), browser.getTimeoutForCondition());
        browser.getDriver().manage().timeouts().implicitlyWait(browser.getTimeoutForCondition(), TimeUnit.SECONDS);
        try {
            wait.until((ExpectedCondition<Boolean>) driver -> {
                try {
                    List<WebElement> list = driver.findElements(locator);
                    for (WebElement el : list) {
                        if (el instanceof RemoteWebElement && el.isDisplayed()) {
                            return true;
                        }
                    }
                } catch (Exception e) {
                    return false;
                }
                return false;
            });
        } catch (Exception e) {
            Logger.getInstance().warn(e.getMessage());
        }
        List<WebElement> list = browser.getDriver().findElements(locator);
        Integer itemNumber = 0;
        for (WebElement el : list) {
            if (el instanceof RemoteWebElement && el.isDisplayed()) {
                try {
                    T elementUI = (T) tClass.getDeclaredConstructor(RemoteWebElement.class, String.class).newInstance(el, String.format("%s [%s]", name, itemNumber));
                    elementsUIList.add(elementUI);
                    itemNumber++;
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    Logger.getInstance().warn(e.getMessage());
                }
            }
        }
        return elementsUIList;
    }
}
