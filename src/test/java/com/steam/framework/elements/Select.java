package com.steam.framework.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class Select extends BaseUIElement {
    /**
     *
     * @param locator
     * @param name
     */
    public Select(final By locator, final String name) {
        super(locator, name);
    }



    /**
     * Select item.
     */
    public void selectByVisibleText(String selectItem) {
        waitElementDisplayed();
        browser.getDriver().getMouse().mouseMove(element.getCoordinates());
        if (browser.getDriver() instanceof JavascriptExecutor) {
            ((JavascriptExecutor)browser.getDriver()).executeScript("arguments[0].style.border='3px solid red'", element);
        }
        new org.openqa.selenium.support.ui.Select(this.element).selectByVisibleText(selectItem);
    };

}
