package com.steam.framework.core;

import static org.testng.AssertJUnit.assertTrue;

import com.steam.framework.browser.Browser;
import com.steam.framework.utils.INameElementForLogger;
import com.steam.framework.utils.LocaleManager;
import com.steam.framework.utils.Logger;
import org.testng.ITestContext;

public abstract class BaseEntity {


	protected static Logger logger = Logger.getInstance();
	protected static LocaleManager localeManager = LocaleManager.getInstance();
	protected static Browser browser = Browser.getInstance();
	protected ITestContext context;

	protected static String getLocale(final String key) {
		return localeManager.getProperty(key);
	}

	protected String extendsMessage(final String message) {
		Class clazz = this.getClass();
		String type = clazz.getName().replace(clazz.getPackageName() + ".","");
		String name = "";
		if (this instanceof INameElementForLogger) {
			name = ((INameElementForLogger) this).getName();
		}
		return String.format("%1$s %2$s %3$s %4$s", type, name, Logger.LOG_DELIMITER, message);
	}

	protected void info(final String message) {
		logger.info(extendsMessage(message));
	}

	protected void warn(final String message) {
		logger.warn(extendsMessage(message));
	}

	protected void error(final String message) {
		logger.error(extendsMessage(message));
	}

	protected void fatal(final String message) {
		logger.fatal(extendsMessage(message));
		assertTrue(message, false);
	}

	public static void logStep(final int step) {
		logger.step(step);
	}

	public void logStep(final int fromStep, final int toStep) {
		logger.step(fromStep, toStep);
	}

	public void doAssert(final Boolean isTrue, final String passMsg,
			final String failMsg) {
		if (isTrue) {
			info(passMsg);
		} else {
			fatal(failMsg);
		}
	}

	public void assertEquals(final Object expected, final Object actual) {
		if (!expected.equals(actual)) {
			fatal("Expected value: '" + expected + "', but was: '" + actual + "'");
		}
	}

	public void assertEquals(final String message, final Object expected, final Object actual) {
		if (!expected.equals(actual)) {
			fatal(message);
		}
	}





}
