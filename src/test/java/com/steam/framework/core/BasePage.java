package com.steam.framework.core;

import java.util.Date;

import com.steam.framework.elements.Label;
import com.steam.framework.utils.INameElementForLogger;
import org.openqa.selenium.By;

public abstract class BasePage extends BaseEntity implements INameElementForLogger {

	protected By titleLocator;
	protected String title;
	protected String name;

	public BasePage(final By locator, final String formTitle) {
		init(locator, formTitle);
		assertIsOpen();
	}

	public String getName() {
		return name;
	}

	private void init(final By locator, final String formTitle) {
		titleLocator = locator;
		title = formTitle;
		name = String.format(getLocale("loc.page") + " '%s'", this.title);
	}

	public void assertIsOpen() {
		long before = new Date().getTime();
		Label elem = new Label(titleLocator, title);
		try {
			elem.waitElementDisplayed();
			long openTime = new Date().getTime() - before;
			
				info(String.format(getLocale("loc.page.appears"), title) + String.format(" in %smsec",openTime));
			
		} catch (Throwable e) {
			fatal(String.format(getLocale("loc.page.not.appears"), title));
		}
	}
}
