package com.steam.framework.utils;

import com.steam.framework.core.BaseEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    File downloads use Curl
    https://www.selenium.dev/documentation/en/worst_practices/file_downloads/
*/
public class FileDownload extends BaseEntity {
    public FileDownload() {

    }

    public void downloadFile(String fileURL, String fileName, int minSizeBytes) {
        try {
            String[] commands = {"curl", "-o", String.format("./%s", fileName), "-k", fileURL, "-w", "%{size_download}", "-#"};
            //String[] commands = {"pwd"};

            String response = this.executeCommand(commands);
            info(response);
            int fileSize = Integer.valueOf(response);
            assert (fileSize >= minSizeBytes);

            commands = new String[]{"rm", fileName};
            response = this.executeCommand(commands);
            info(response);
        } catch (Error | IOException e) {
            info(e.toString());
            assert (false);
        }

    }

    private String executeCommand(String[] commands) throws IOException {
        Process process = Runtime.getRuntime().exec(commands);
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(process.getInputStream()));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }

        return response.toString();
    }
}
