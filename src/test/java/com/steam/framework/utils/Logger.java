package com.steam.framework.utils;


import org.testng.Assert;
import org.testng.Reporter;

public final class Logger {
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);
    private static Logger instance = null;
    public static final String LOG_DELIMITER = "->";

    protected static String getLocale(final String key) {
        return LocaleManager.getInstance().getProperty(key);
    }

    private Logger() {
    }

    public static synchronized Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void step(final int step) {
        logStepMessage(getLocale("loc.logger.step") + String.valueOf(step));
    }

    public void step(final int fromStep, final int toStep) {
        logStepMessage(getLocale("loc.logger.steps") + String.valueOf(fromStep) + "-" + String.valueOf(toStep));
    }

    private void logStepMessage(final String msg) {
        info(String.format("******* %1$s *******", msg));
    }

    public void fatal(final String message) {
        String msg = message;
        logger.fatal(message);
        msg = "<div class=\"failedConfig\">" + msg + "</div>"; // red color from reportng css
        Reporter.log(msg + "<br>");
        Assert.assertTrue(false);
    }

    public void error(final String message) {
        String msg = message;
        logger.error(message);
        msg = "<div class=\"failedConfig\">" + msg + "</div>"; // red color from reportng css
        Reporter.log(msg + "<br>");
    }

    public void warn(final String message) {
        String msg = message;
        logger.warn(message);
        msg = "<div class=\"skipped\">" + msg + "</div>"; // yellow color from reportng css
        Reporter.log(msg + "<br>");
    }

    public void info(final String message) {
        logger.info(message);
        Reporter.log(message + "<br>");
    }






}
