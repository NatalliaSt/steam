package com.steam.framework.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;


public final class PropertiesManager {

	private Properties properties = new Properties();

	public PropertiesManager(final String resourceName) {
		properties = appendFromResource(properties, resourceName);
	}

	public PropertiesManager(final String defaultResourceName, final String resourceName) {
		this(defaultResourceName);
		properties = appendFromResource(new Properties(properties), resourceName);
	}

	public String getProperty(final String key) {
		return properties.getProperty(key);
	}

	private Properties appendFromResource(final Properties objProperties, final String resourceName) {
			try {
				InputStream inStream = this.getClass().getClassLoader().getResourceAsStream(resourceName);
				objProperties.load(new InputStreamReader(inStream, StandardCharsets.UTF_8));
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println(String.format("Resource \"%1$s\" could not be found", resourceName));
			}
		return objProperties;
	}



}
