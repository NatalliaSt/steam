package com.steam.tests;

import com.steam.framework.core.BaseTest;

import com.steam.framework.utils.FileDownload;
import com.steam.pages.*;
import org.testng.annotations.Test;

public class SteamTest extends BaseTest {

    @Test
    public void runTest() {
        logStep(1);
        browser.navigateToURL(browser.startUrl);
        MainPage mainPage = new MainPage();
        logStep(2);
        mainPage.choseLanguage(getLocale("loc.main-form.language"));
        logStep(3);
        mainPage.navigateMenu(getLocale("loc.main-form.menu.item"),getLocale("loc.main-form.submenu.item"));
        BrowsingPage browsingPage = new BrowsingPage();
        logStep(4);
        browsingPage.choseMaxDiscountRecommendedSpecialsGame();
        logStep(5);
        AgeVerifyPage ageVerifyPage = new AgeVerifyPage();
        ageVerifyPage.enterDOB();
        logStep(6);
        AppPage appPage = new AppPage();
        appPage.gamePageAssert();
        logStep(7);
        appPage.openInstallSteamPage();
        AboutPage aboutPage = new AboutPage();
        logStep(8);
        String downloadURL = aboutPage.getDownloadURL();
        FileDownload fileDownload = new FileDownload();
        fileDownload.downloadFile(downloadURL, getLocale("loc.download.file-name"), Integer.valueOf(getLocale("loc.download.min-size-bytes")));
    }
}
